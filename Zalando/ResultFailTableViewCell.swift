//
//  ResultFailTableViewCell.swift
//  Zalando
//
//  Created by Vladimir Urbano on 04-06-16.
//  Copyright © 2016 Vladimir Urbano. All rights reserved.
//

import UIKit

class ResultFailTableViewCell: UITableViewCell {
	@IBOutlet weak var testCaseLabel: UILabel!
	@IBOutlet weak var testCaseImage: UIImageView!
	@IBOutlet weak var impossibleImage: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
