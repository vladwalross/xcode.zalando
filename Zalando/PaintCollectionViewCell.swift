//
//  PaintCollectionViewCell.swift
//  Zalando
//
//  Created by Vladimir Urbano on 31-05-16.
//  Copyright © 2016 Vladimir Urbano. All rights reserved.
//

import UIKit

class PaintCollectionViewCell: UICollectionViewCell {
	@IBOutlet weak var paintImage: UIImageView!
	@IBOutlet weak var paintLabel: UILabel!
	@IBOutlet weak var typeImage: UIImageView!
	@IBOutlet weak var typeLabel: UILabel!
}
