//
//  InputViewController.swift
//  Zalando
//
//  Created by Vladimir Urbano on 30-05-16.
//  Copyright © 2016 Vladimir Urbano. All rights reserved.
//

import UIKit
import PaintShop

class InputViewController: UIViewController {
	// MARK: - Properties
	@IBOutlet weak var testCasesLabel: UILabel!
	@IBOutlet weak var paintColorsLabel: UILabel!
	@IBOutlet weak var customersLabel: UILabel!
	@IBOutlet weak var testCasesCollectionView: UICollectionView!
	@IBOutlet weak var testCaseView: UIView!
	@IBOutlet weak var customersCollectionView: UICollectionView!
	@IBOutlet weak var customerLikesCollectionView: UICollectionView!
	@IBOutlet weak var testCaseSeparator: UIView!
	@IBOutlet weak var customerSeparator: UIView!
	@IBOutlet weak var scrollView: UIScrollView!
	
	var input: Input!
	
	private var selectedIndexPath = Dictionary<String, NSIndexPath>()
	private var selectedColorIndexPath = Dictionary<Int, Dictionary<Int, Dictionary<Int, Bool>>>()
	private var selectedTypeIndexPath = Dictionary<Int, Dictionary<Int, Dictionary<Int, Bool?>>>()
	private var paintShop: PaintShop!
	
	// MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
		
		if let image = UIImage(named: "ButtonBatches") {
			let button = UIButton(type: .Custom)
			button.setImage(image, forState: .Normal)
			button.addTarget(self, action: #selector(InputViewController.makeBatches(_:)), forControlEvents: .TouchUpInside)
			button.frame = CGRectMake(0, 0, 40, 40)
			let barButton = UIBarButtonItem(customView: button)
			self.navigationItem.rightBarButtonItem = barButton
		}
		
		self.navigationItem.rightBarButtonItems?.removeLast()
		
		self.testCasesCollectionView.dataSource = self
		self.testCasesCollectionView.delegate = self
		
		self.customersCollectionView.dataSource = self
		self.customersCollectionView.delegate = self
		
		self.customerLikesCollectionView.dataSource = self
		self.customerLikesCollectionView.delegate = self
		
		self.resetData()
    }
	
	override func viewDidAppear(animated: Bool) {
		super.viewDidAppear(animated)
		
		self.reloadData()
	}

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
	
	// MARK: - Actions
	private func changeLabelNumber(sender: UIButton, possitive: Bool) {
		switch sender.tag {
		case 1:
			var new = self.input.cases.count
			if possitive {
				new += 1
			} else {
				new -= 1
			}
			
			if new.keepInRange(0, max: 100) {
				if possitive {
					let testCase = TestCase(paintColors: 0)
					self.input.cases.append(testCase)
				} else {
					self.removeLastTestCase()
				}
			}
		case 2:
			var new = self.input.cases[self.testCasesCollectionView.tag - 1].paintColors
			if possitive {
				new += 1
			} else {
				new -= 1
			}
			
			if self.input.cases.count > 5 {
				new.keepInRange(0, max: 10)
			} else {
				new.keepInRange(0, max: 2000)
			}

			self.input.cases[self.testCasesCollectionView.tag - 1].paintColors = new
		case 3:
			var new = self.input.cases[self.testCasesCollectionView.tag - 1].customers.count
			if possitive {
				new += 1
			} else {
				new -= 1
			}
			
			var kept = true
			if self.testCasesLabel.tag > 5 {
				kept = new.keepInRange(0, max: 100)
			} else {
				kept = new.keepInRange(0, max: 2000)
			}
			
			if kept {
				if possitive {
					let customer = Customer()
					self.input.cases[self.testCasesCollectionView.tag - 1].customers.append(customer)
				} else {
					self.input.cases[self.testCasesCollectionView.tag - 1].customers.removeLast()
				}
			}
		case 4:
			var new = self.input.cases[self.testCasesCollectionView.tag - 1].customers[self.customersCollectionView.tag - 1].likes.count
			if possitive {
				new += 1
			} else {
				new -= 1
			}
			
			new.keepInMinRange(0)
			
			if possitive {
				let customerLike = CustomerLike(paintColor: -1, colorIsMatte: false)
				self.input.cases[self.testCasesCollectionView.tag - 1].customers[self.customersCollectionView.tag - 1].likes.append(customerLike)
			} else {
				self.input.cases[self.testCasesCollectionView.tag - 1].customers[self.customersCollectionView.tag - 1].likes.removeLast()
			}
			
			if self.sumAllTValues() > 3000 {
				self.alert("The sum of all customer likes cannot be more than 3 000")
				return
			}
		default:
			break
		}
		
		self.reloadData()
	}
	
	@IBAction func less(sender: UIButton) {
		self.changeLabelNumber(sender, possitive: false)
	}
	
	@IBAction func more(sender: UIButton) {
		self.changeLabelNumber(sender, possitive: true)
	}

	func tapCell(sender: UITapGestureRecognizer) {
		if let image = sender.view, view = image.superview, cell = view.superview {
			
			self.initSelectedIndices()
			
			switch image.tag {
			case 1:
				let indexPath = NSIndexPath(forItem: cell.tag, inSection: 0)
				
				if let selected0 = self.selectedColorIndexPath[self.testCasesCollectionView.tag - 1], selected1 = selected0[self.customersCollectionView.tag - 1], selected = selected1[cell.tag] where selected {
					self.selectedColorIndexPath[self.testCasesCollectionView.tag - 1]![self.customersCollectionView.tag - 1]![cell.tag] = false
					self.selectedTypeIndexPath[self.testCasesCollectionView.tag - 1]![self.customersCollectionView.tag - 1]![cell.tag] = nil
				} else {
					self.selectedColorIndexPath[self.testCasesCollectionView.tag - 1]![self.customersCollectionView.tag - 1]![cell.tag] = true
					self.selectedTypeIndexPath[self.testCasesCollectionView.tag - 1]![self.customersCollectionView.tag - 1]![cell.tag] = false
				}
				
				self.customerLikesCollectionView.reloadItemsAtIndexPaths([indexPath])
			case 2:
				if let selectedColor0 = self.selectedColorIndexPath[self.testCasesCollectionView.tag - 1], selectedColor1 = selectedColor0[self.customersCollectionView.tag - 1], selectedColor = selectedColor1[cell.tag] where selectedColor {
						let indexPath = NSIndexPath(forItem: cell.tag, inSection: 0)
						if let selected0 = self.selectedTypeIndexPath[self.testCasesCollectionView.tag - 1], selected1 = selected0[self.customersCollectionView.tag - 1], selected = selected1[cell.tag], s = selected where s {
							self.selectedTypeIndexPath[self.testCasesCollectionView.tag - 1]![self.customersCollectionView.tag - 1]![cell.tag] = false
						} else {
							if self.canAddMatte(self.testCasesCollectionView.tag - 1, customer: self.customersCollectionView.tag - 1) {
								self.selectedTypeIndexPath[self.testCasesCollectionView.tag - 1]![self.customersCollectionView.tag - 1]![cell.tag] = true
							} else {
								self.alert("A customer might like only one matte")
							}
						}
						
						self.customerLikesCollectionView.reloadItemsAtIndexPaths([indexPath])
					} else {
						self.selectedTypeIndexPath[self.testCasesCollectionView.tag - 1]![self.customersCollectionView.tag - 1]![cell.tag] = nil
					}
			default:
				break
			}
		}
	}
	
	@IBAction func makeBatches(sender: UIBarButtonItem) {
		for testCase in 0 ..< self.input.cases.count {
			for customer in 0 ..< self.input.cases[testCase].customers.count {
				self.input.cases[testCase].customers[customer].likes.removeAll()
			}
		}
		
		var errors = Dictionary<String, Array<String>>()
		errors["TestCase"] = Array<String>()
		errors["Customer"] = Array<String>()
		errors["Color"] = Array<String>()
		errors["Like"] = Array<String>()
		if self.input.cases.count == 0 {
			errors["TestCase"]!.append("There should be at least one test case.")
 		}
		
		for testCase in 0 ..< self.input.cases.count {
			if self.input.cases[testCase].customers.count == 0 {
				errors["Customer"]!.append("Test case #\(testCase + 1) should have at least one customer.")
			}
			
			if self.input.cases[testCase].paintColors == 0 {
				errors["Color"]!.append("Test case #\(testCase + 1) should have at least one paint color.")
			}
			
			for customer in 0 ..< self.input.cases[testCase].customers.count {
				for color in 0 ..< self.input.cases[testCase].paintColors {
					if let selected0 = self.selectedColorIndexPath[testCase], selected1 = selected0[customer], selected = selected1[color] where selected {
						if let selectedType0 = self.selectedTypeIndexPath[testCase], selectedType1 = selectedType0[customer], selectedType = selectedType1[color], s = selectedType where s {
							self.input.cases[testCase].customers[customer].likes.append(CustomerLike(paintColor: color, colorIsMatte: true))
						} else {
							self.input.cases[testCase].customers[customer].likes.append(CustomerLike(paintColor: color, colorIsMatte: false))
						}
					}
				}
				
				if self.input.cases[testCase].customers[customer].likes.count == 0 {
					errors["Like"]!.append("Customer #\(customer + 1) in test case #\(testCase + 1) should have at least one color they like.")
				}
			}
		}
		
		var message = ""
		var c = 0
		if let testCases = errors["TestCase"] where message.isEmpty && testCases.count > 0 {
			for item in testCases {
				c += 1
				message += "\n\n\(item)"
				if c >= 3 {
					break
				}
			}
			message = "There are some errors: \(message)"
		}
		
		if let customers = errors["Customer"] where message.isEmpty && customers.count > 0 {
			for item in customers {
				c += 1
				message += "\n\n\(item)"
				if c >= 3 {
					break
				}
			}
			message = "There are some errors: \(message)"
		}
		
		if let colors = errors["Color"] where message.isEmpty && colors.count > 0 {
			for item in colors {
				c += 1
				message += "\n\n\(item)"
				if c >= 3 {
					break
				}
			}
			message = "There are some errors: \(message)"
		}
		
		if let likes = errors["Like"] where message.isEmpty && likes.count > 0 {
			for item in likes {
				c += 1
				message += "\n\n\(item)"
				if c >= 3 {
					break
				}
			}
			message = "There are some errors: \(message)"
		}
		
		if message.isEmpty {
			self.paintShop = PaintShop()
			if self.paintShop.encodeInput(self.input) {
				self.performSegueWithIdentifier("OutputStoryboardSegue", sender: nil)
			} else {
				self.alert("There was an error while trying to build the input file")
			}
		} else {
			self.alert(message)
		}
	}
	
	@IBAction func reset(sender: UIBarButtonItem) {
		let alert = UIAlertController(title: "Reset data", message: "Are you sure you want to reset the form?", preferredStyle: .Alert)
		alert.addAction(UIAlertAction(title: "Reset", style: .Destructive, handler: { (action) in
			self.resetData()
			self.reloadData()
		}))
		alert.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: nil))
		self.presentViewController(alert, animated: true, completion: nil)
	}
	
    // MARK: - Navigation
	override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    }
	
	// MARK: - Private methods
	private func reloadData() {
		self.view.hideHelpText()
		self.testCasesLabel.text = self.input.cases.count.testCaseCountText
		
		if self.input.cases.count == 0 {
			self.testCasesCollectionView.hidden = true
			self.testCasesLabel.superview?.showHelpText("Set test case count")
		} else {
			self.testCasesCollectionView.hidden = false
			self.testCasesCollectionView.reloadData()
			if self.testCasesCollectionView.tag == 0 {
				self.testCasesCollectionView.showHelpText("Choose test case")
			}
		}
		
		if self.input.cases.count > 0 && self.testCasesCollectionView.tag > 0 {
			self.paintColorsLabel.text = self.input.cases[self.testCasesCollectionView.tag - 1].paintColors.paintColorCountText
			self.customersLabel.text = self.input.cases[self.testCasesCollectionView.tag - 1].customers.count.customerCountText
			self.testCaseView.hidden = false
			self.testCaseSeparator.hidden = false
		} else {
			self.testCaseView.hidden = true
			self.testCaseSeparator.hidden = true
		}
		
		if self.input.cases.count > 0 && self.testCasesCollectionView.tag > 0 && self.input.cases[self.testCasesCollectionView.tag - 1].customers.count > 0 {
			self.customersCollectionView.hidden = false
			self.customersCollectionView.reloadData()
			if self.customersCollectionView.tag == 0 {
				self.customersCollectionView.showHelpText("Choose customer")
			}
		} else {
			self.customersCollectionView.hidden = true
			self.testCaseView.showHelpText("Set paint and customer count")
		}
		
		if self.input.cases.count > 0 && self.testCasesCollectionView.tag > 0 && self.input.cases[self.testCasesCollectionView.tag - 1].customers.count > 0 && self.customersCollectionView.tag > 0 {
			self.customerLikesCollectionView.hidden = false
			self.customerSeparator.hidden = false
			self.customerLikesCollectionView.reloadData()
		} else {
			self.customerSeparator.hidden = true
			self.customerLikesCollectionView.hidden = true
		}
	}
	
	private func sumAllTValues() -> Int {
		var sum = 0
		for testCase in self.input.cases {
			for customer in testCase.customers {
				sum += customer.likes.count
			}
		}
		
		return sum
	}
	
	private func removeLastTestCase() {
		if let selected = self.selectedIndexPath["TestCase"] where selected.compare(NSIndexPath(forItem: self.input.cases.count - 1, inSection: 0)) == .OrderedSame {
			self.selectedIndexPath["TestCase"] = nil
			self.testCasesCollectionView.tag = 0
		}
		self.input.cases.removeLast()
	}
	
	private func removeLastPaintColor() {
		self.customerLikesCollectionView.reloadData()
	}
	
	private func removeLastCustomer(testCaseId: Int) {
		if let selected = self.selectedIndexPath["Customer"] where selected.compare(NSIndexPath(forItem: self.input.cases[testCaseId].customers.count - 1, inSection: 0)) == .OrderedSame {
			self.selectedIndexPath["Customer"] = nil
			self.customersCollectionView.tag = 0
		}
		self.input.cases[testCaseId].customers.removeLast()
	}
	
	private func removeLastCustomerLike(testCaseId: Int, customerId: Int) {
		self.input.cases[testCaseId].customers[customerId].likes.removeLast()
		self.reloadData()
	}
	
	private func initSelectedIndices() {
		if let selected0 = self.selectedColorIndexPath[self.testCasesCollectionView.tag - 1] {
			if selected0[self.customersCollectionView.tag - 1] == nil {
				self.selectedColorIndexPath[self.testCasesCollectionView.tag - 1]![self.customersCollectionView.tag - 1] = Dictionary<Int, Bool>()
			}
		} else {
			self.selectedColorIndexPath[self.testCasesCollectionView.tag - 1] = Dictionary<Int, Dictionary<Int, Bool>>()
		}
		
		if let selected0 = self.selectedTypeIndexPath[self.testCasesCollectionView.tag - 1] {
			if selected0[self.customersCollectionView.tag - 1] == nil {
				self.selectedTypeIndexPath[self.testCasesCollectionView.tag - 1]![self.customersCollectionView.tag - 1] = Dictionary<Int, Bool?>()
			}
		} else {
			self.selectedTypeIndexPath[self.testCasesCollectionView.tag - 1] = Dictionary<Int, Dictionary<Int, Bool?>>()
		}
	}
	
	private func resetData() {
		self.selectedIndexPath["TestCase"] = nil
		self.selectedIndexPath["Customer"] = nil
		self.selectedColorIndexPath = Dictionary<Int, Dictionary<Int, Dictionary<Int, Bool>>>()
		self.selectedTypeIndexPath = Dictionary<Int, Dictionary<Int, Dictionary<Int, Bool?>>>()
		
		self.testCasesCollectionView.tag = 0
		self.customersCollectionView.tag = 0
		self.customerLikesCollectionView.tag = 0
		
		self.input = Input()
	}
	
	private func canAddMatte(testCase: Int, customer: Int) -> Bool {
		for color in 0 ..< self.input.cases[testCase].paintColors {
			if let selected0 = self.selectedColorIndexPath[testCase], selected1 = selected0[customer], selected = selected1[color] where selected {
				if let selectedType0 = self.selectedTypeIndexPath[testCase], selectedType1 = selectedType0[customer], selectedType = selectedType1[color], s = selectedType where s {
					return false
				}
			}
		}
		
		return true
	}
	
	private func reloadFromFS() {
		
	}
}

extension InputViewController: UICollectionViewDataSource, UICollectionViewDelegate {
	func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
		return 1
	}
	
	func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		switch collectionView {
		case self.testCasesCollectionView :
			return self.input.cases.count
		case self.customersCollectionView :
			if self.testCasesCollectionView.tag > 0 {
				return self.input.cases[self.testCasesCollectionView.tag - 1].customers.count
			}
		case self.customerLikesCollectionView :
			if self.testCasesCollectionView.tag > 0 {
				return self.input.cases[self.testCasesCollectionView.tag - 1].paintColors
			}
		default:
			return 0
		}
		
		return 0
	}
	
	func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
		switch collectionView {
		case self.testCasesCollectionView :
			let cell = collectionView.dequeueReusableCellWithReuseIdentifier("TestCaseCell", forIndexPath: indexPath) as! TestCaseCollectionViewCell
			
			cell.label.text = "\(indexPath.row + 1)"
			let r = (indexPath.row % 5) + 1
			if let image = UIImage(named: "TestCase\(r)") {
				cell.image.image = image
			}
			
			if let index = self.selectedIndexPath["TestCase"] where indexPath.compare(index) == .OrderedSame {
				cell.image.roundCorners(true)
			} else {
				cell.image.roundCorners()
			}
			
			return cell
		case self.customersCollectionView :
			let cell = collectionView.dequeueReusableCellWithReuseIdentifier("CustomerCell", forIndexPath: indexPath) as! CustomerCollectionViewCell
			
			cell.label.text = "\(indexPath.row + 1)"
			let r = (indexPath.row % 5) + 1
			if let image = UIImage(named: "Customer\(r)") {
				cell.image.image = image
			}
			
			if let index = self.selectedIndexPath["Customer"] where indexPath.compare(index) == .OrderedSame {
				cell.image.roundCorners(true)
			} else {
				cell.image.roundCorners()
			}
			
			return cell
		case self.customerLikesCollectionView :
			let cell = collectionView.dequeueReusableCellWithReuseIdentifier("PaintCell", forIndexPath: indexPath) as! PaintCollectionViewCell
			
			self.initSelectedIndices()
			
			cell.tag = indexPath.row
			
			cell.paintLabel.text = "\(indexPath.row + 1)"
			
			let paintGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(InputViewController.tapCell(_:)))
			cell.paintImage.addGestureRecognizer(paintGestureRecognizer)
			
			var r = (indexPath.row % 11) + 1
			if let image = UIImage(named: "Color\(r)") {
				cell.paintImage.image = image
			}
			
			if let selected0 = self.selectedColorIndexPath[self.testCasesCollectionView.tag - 1], selected1 = selected0[self.customersCollectionView.tag - 1], selected = selected1[indexPath.row] where selected {
				cell.paintImage.roundCorners(true)
			} else {
				cell.paintImage.roundCorners()
			}
			
			let typeGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(InputViewController.tapCell(_:)))
			cell.typeImage.addGestureRecognizer(typeGestureRecognizer)
			
			r = (indexPath.row % 3) + 1
			var named = "NoColor"
			if let selected0 = self.selectedTypeIndexPath[self.testCasesCollectionView.tag - 1], selected1 = selected0[self.customersCollectionView.tag - 1], selected = selected1[indexPath.row] {
				if let s = selected {
					if s {
						cell.typeLabel.text = "Matte"
						named = "Matte"
					} else {
						cell.typeLabel.text = "Glossy"
						named = "Glossy"
					}
				} else {
					cell.typeLabel.text = ""
					named = "NoColor"
				}
			} else {
				cell.typeLabel.text = ""
				named = "NoColor"
			}
			
			if let image = UIImage(named: "\(named)\(r)") {
				cell.typeImage.image = image
			}
			
			if let selected0 = self.selectedColorIndexPath[self.testCasesCollectionView.tag - 1], selected1 = selected0[self.customersCollectionView.tag - 1], selected = selected1[indexPath.row] where selected {
				cell.typeImage.roundCorners(true)
			} else {
				cell.typeImage.roundCorners()
			}
			
			return cell
		default:
			return UICollectionViewCell()
		}
	}
	
	func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
		var key: String? = nil
		switch collectionView {
		case self.testCasesCollectionView :
			key = "TestCase"
		case self.customersCollectionView :
			key = "Customer"
			var rect = self.customerLikesCollectionView.bounds
			rect = self.customerLikesCollectionView.convertRect(rect, toView: self.scrollView)
			rect.origin.y += 30
			self.scrollView.scrollRectToVisible(rect, animated: true)
		default:
			break
		}
		
		var indexes = [indexPath]
		
		if let _key = key {
			if let index = self.selectedIndexPath[_key] {
				if indexPath.compare(index) != .OrderedSame {
					indexes.append(index)
					self.selectedIndexPath[_key] = indexPath
					switch collectionView {
					case self.testCasesCollectionView :
						self.selectedIndexPath["Customer"] = nil
						
						self.customersCollectionView.tag = 0
					case self.customersCollectionView :
						break
					default:
						break
					}
				}
			} else {
				self.selectedIndexPath[_key] = indexPath
			}
			
			collectionView.reloadItemsAtIndexPaths(indexes)
		}
		
		collectionView.tag = indexPath.row + 1
		self.reloadData()
	}
}
