//
//  ResultSuccessCollectionViewCell.swift
//  Zalando
//
//  Created by Vladimir Urbano on 04-06-16.
//  Copyright © 2016 Vladimir Urbano. All rights reserved.
//

import UIKit

class ResultSuccessCollectionViewCell: UICollectionViewCell {
	@IBOutlet weak var paintImage: UIImageView!
	@IBOutlet weak var paintLabel: UILabel!
	@IBOutlet weak var typeImage: UIImageView!
	@IBOutlet weak var typeLabel: UILabel!
}
