//
//  CustomerCollectionViewCell.swift
//  Zalando
//
//  Created by Vladimir Urbano on 31-05-16.
//  Copyright © 2016 Vladimir Urbano. All rights reserved.
//

import UIKit

class CustomerCollectionViewCell: UICollectionViewCell {
	@IBOutlet weak var image: UIImageView!
	@IBOutlet weak var label: UILabel!
}
