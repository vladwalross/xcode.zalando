//
//  TestCaseCollectionViewCell.swift
//  Zalando
//
//  Created by Vladimir Urbano on 30-05-16.
//  Copyright © 2016 Vladimir Urbano. All rights reserved.
//

import UIKit

class TestCaseCollectionViewCell: UICollectionViewCell {
	@IBOutlet weak var image: UIImageView!
	@IBOutlet weak var label: UILabel!
	
}
