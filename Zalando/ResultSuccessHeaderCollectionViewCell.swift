//
//  ResultSuccessHeaderCollectionViewCell.swift
//  Zalando
//
//  Created by Vladimir Urbano on 08-06-16.
//  Copyright © 2016 Vladimir Urbano. All rights reserved.
//

import UIKit

class ResultSuccessHeaderCollectionViewCell: UICollectionViewCell {
	@IBOutlet weak var label: UILabel!
	@IBOutlet weak var image: UIImageView!
    
}
