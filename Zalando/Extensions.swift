//
//  Extensions.swift
//  Zalando
//
//  Created by Vladimir Urbano on 30-05-16.
//  Copyright © 2016 Vladimir Urbano. All rights reserved.
//

import UIKit

extension Int {
	var paintColorCountText: String {
		return "\(self) paint colors"
	}
	
	var testCaseCountText: String {
		return "\(self) test cases"
	}
	
	var customerCountText: String {
		return "\(self) customers"
	}
	
	var customerLikeCountText: String {
		return "\(self) customer likes"
	}
	
	mutating func keepInRange(min: Int, max: Int) -> Bool {
		if self < min {
			self = min
			return false
		}
		
		if self > max {
			self = max
			return false
		}
		
		return true
	}
	
	mutating func keepInMinRange(min: Int) -> Bool {
		if self < min {
			self = min
			return false
		}
		
		return true
	}
	
	mutating func keepInMaxRange(max: Int) -> Bool {
		if self > max {
			self = max
			return false
		}
		
		return true
	}
	
	static func random(max: Int) -> Int {
		let r = Int(arc4random_uniform(UInt32(max)))
		return r
	}
}

extension String {
	var caseNumber: String {
		if let s = self.componentsSeparatedByString(":").first {
			return s
		}
		return self
	}
	
	var caseResults: [String] {
		if let s = self.componentsSeparatedByString(":").last {
			var a = s.componentsSeparatedByString(" ")
			if let f = a.first where f == "" || f == " " {
				a.removeFirst()
			}
			return a
		}
		
		return [String]()
	}
}

extension UIView {
	func addBorders() {
		self.layer.borderColor = UIColor.blackColor().CGColor
		self.layer.borderWidth = 1
		self.clipsToBounds = false
	}
	
	func roundCorners() {
		self.roundCorners(false, radius: 15)
	}
	
	func roundCorners(bold: Bool) {
		self.roundCorners(bold, radius: 15)
	}
	
	func roundCorners(bold: Bool, radius: CGFloat) {
		self.layer.cornerRadius = radius
		self.layer.borderWidth = 1
		self.layer.borderColor = UIColor.blackColor().CGColor
		self.clipsToBounds = true
		
		if bold {
			self.transform = CGAffineTransformIdentity
		} else {
			self.transform = CGAffineTransformMakeScale(0.7, 0.7)
		}
	}
	
	func showHelpText(text: String) {
		if self.hidden {
			return
		}
		
		let label = UILabel()
		label.text = text
		label.font = UIFont.systemFontOfSize(15, weight: UIFontWeightUltraLight)
		label.textColor = UIColor.purpleColor()
		label.textAlignment = .Center
		label.frame.size.width = self.frame.width
		label.frame.size.height = 24
		label.frame.origin.x = self.frame.origin.x
		label.frame.origin.y = self.frame.origin.y + self.frame.size.height
		label.tag = 777
		self.superview?.addSubview(label)
	}
	
	func hideHelpText() {
		self.hideHelpText(self)
	}
	
	func hideHelpText(view: UIView) {
		for v in view.subviews {
			if v.subviews.count > 0 {
				self.hideHelpText(v)
			} else if let label = v as? UILabel where label.tag == 777 {
				label.hidden = true
				label.removeFromSuperview()
			}
		}
	}
}

extension UIViewController {
	func alert(message: String) {
		if let storyboard = self.storyboard, controller = storyboard.instantiateViewControllerWithIdentifier("AlertViewController") as? AlertViewController {
			controller.message = message
			self.presentViewController(controller, animated: true, completion: nil)
		}
	}
}
