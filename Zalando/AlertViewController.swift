//
//  AlertViewController.swift
//  Zalando
//
//  Created by Vladimir Urbano on 07-06-16.
//  Copyright © 2016 Vladimir Urbano. All rights reserved.
//

import UIKit

class AlertViewController: UIViewController {
	// MARK: - Properties
	@IBOutlet weak var label: UILabel!
	
	var message: String?

	// MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()

        self.label.text = message
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
	
	// MARK: - Actions
	@IBAction func close(sender: UIButton) {
		self.dismissViewControllerAnimated(true, completion: nil)
	}

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
