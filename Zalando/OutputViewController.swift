//
//  OutputViewController.swift
//  Zalando
//
//  Created by Vladimir Urbano on 04-06-16.
//  Copyright © 2016 Vladimir Urbano. All rights reserved.
//

import UIKit
import PaintShop

class OutputViewController: UIViewController {
	// MARK: - Properties
	@IBOutlet weak var tableView: UITableView!
	
	var output: [String]!
	
	private var paintShop: PaintShop!

	// MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()

		self.paintShop = PaintShop()
        self.output = self.paintShop.makeBatches()
		
		self.tableView.dataSource = self
		self.tableView.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Navigation

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
		if let destination = segue.destinationViewController as? InputViewController {
			do {
				destination.input = try self.paintShop.decodeInput()
			} catch {
				
			}
		}
    }
}

extension OutputViewController: UITableViewDelegate, UITableViewDataSource {
	func numberOfSectionsInTableView(tableView: UITableView) -> Int {
		return 1
	}
	
	func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return self.output.count
	}
	
	func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		if self.output[indexPath.row].containsString("IMPOSSIBLE") {
			let cell = tableView.dequeueReusableCellWithIdentifier("FailCell", forIndexPath: indexPath) as! ResultFailTableViewCell
			
			cell.testCaseLabel.text = self.output[indexPath.row].caseNumber
			var r = (indexPath.row % 5) + 1
			if let image = UIImage(named: "TestCase\(r)") {
				cell.testCaseImage.image = image
				cell.testCaseImage.roundCorners(true, radius: 5)
			}
			
			r = (indexPath.row % 3) + 1
			if let image = UIImage(named: "NoColor\(r)") {
				cell.impossibleImage.image = image
				cell.impossibleImage.roundCorners(true, radius: 5)
			}
			
			return cell
		} else {
			let cell = tableView.dequeueReusableCellWithIdentifier("SuccessCell", forIndexPath: indexPath) as! ResultSuccessTableViewCell
			
			cell.collectionView.dataSource = self
			cell.collectionView.delegate = self
			cell.collectionView.tag = indexPath.row
			
			return cell
		}
	}
	
	func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
		if self.output[indexPath.row].containsString("IMPOSSIBLE") {
			return 56
		}
		
		return 138
	}
}

extension OutputViewController: UICollectionViewDelegate, UICollectionViewDataSource {
	func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
		return 1
	}
	
	func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return self.output[collectionView.tag].caseResults.count + 1
	}
	
	func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
		if indexPath.row == 0 {
			let cell = collectionView.dequeueReusableCellWithReuseIdentifier("Header", forIndexPath: indexPath) as! ResultSuccessHeaderCollectionViewCell
			
			cell.label.text = self.output[collectionView.tag].caseNumber
			
			let r = (collectionView.tag % 5) + 1
			if let image = UIImage(named: "TestCase\(r)") {
				cell.image.image = image
				cell.image.roundCorners(true, radius: 5)
			}
			
			return cell
		} else {
			let cell = collectionView.dequeueReusableCellWithReuseIdentifier("Cell", forIndexPath: indexPath) as! ResultSuccessCollectionViewCell
			
			cell.paintLabel.text = "\(indexPath.row)"
			var r = ((indexPath.row - 1) % 11) + 1
			if let image = UIImage(named: "Color\(r)") {
				cell.paintImage.image = image
				cell.paintImage.roundCorners(true)
			}
			
			r = ((indexPath.row - 1) % 3) + 1
			var named = "Glossy"
			
			if self.output[collectionView.tag].caseResults[indexPath.row - 1] == "1" {
				cell.typeLabel.text = "Matte"
				named = "Matte"
			} else {
				cell.typeLabel.text = "Glossy"
				named = "Glossy"
			}
			
			if let image = UIImage(named: "\(named)\(r)") {
				cell.typeImage.image = image
				cell.typeImage.roundCorners(true)
			}
			
			return cell
		}
	}
	
	func collectionView(collectionView: UICollectionView,
	                    layout collectionViewLayout: UICollectionViewLayout,
	                           sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
		if indexPath.row == 0 {
			return CGSizeMake(120, 128)
		}
		
		return CGSizeMake(60, 128)
	}
}
